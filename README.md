# Surabi - Admin

## Steps To Run

1. [Surabi API Repository](https://gitlab.com/elansakthivel2000/surabi-api) Refer this repository's README file on how to run the server. (if you already have this cloned in your system, pull the master branch to get all the latest commits).
2. After starting the server, clone this angular app.
3. Run npm install inside the root directory.
4. Run npm start
5. Please make sure that the server is running the entire time while you're testing the angular app.

## Admin User Credentials

1. By design in my application there's no sign up process for admin, Admins have to be directly created from backend.
2. If you've followed the instructions provided in the Surabi API Repository to setup the server, you would have two admins users injected into the database.
3. Username:- Elan Password:- elan#1234, Username:- Pranav Password:- pranav#1234
4. You can use the above mentioned users to test the admin operations (Provided the server is setup correctly)
