import { Injectable } from '@angular/core';
import {
    HttpClient,
    HttpErrorResponse,
    HttpHeaders,
} from '@angular/common/http';

import User from '../models/User';
import { BehaviorSubject, Observable, throwError } from 'rxjs';

import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
@Injectable({
    providedIn: 'root',
})
export class AuthService {
    user!: User;
    private currentUserSubject = new BehaviorSubject<any>(null); // initializing with no user object since logged out

    constructor(private http: HttpClient, private router: Router) {
        const userInfo = localStorage.getItem('userInfo');
        if (userInfo) {
            this.currentUserSubject.next(JSON.parse(userInfo));
            return;
        }
        this.router.navigate(['/']);
    }

    errorHandler(error: HttpErrorResponse) {
        return throwError(
            error.error.message ||
                'Something went wrong. Please Try again later.'
        );
    }

    login(username: string, password: string): Observable<User> {
        return this.http
            .post<User>('http://localhost:8080/api/users/login', {
                username,
                password,
            })
            .pipe(
                map((userInfo) => {
                    this.currentUserSubject.next(userInfo);
                    localStorage.setItem('userInfo', JSON.stringify(userInfo));
                    return userInfo;
                })
            )
            .pipe(catchError(this.errorHandler));
    }

    logout() {
        this.getCurrentUser().subscribe((data: User) => {
            this.user = data;
        });
        return this.http
            .post(
                'http://localhost:8080/api/users/logout',
                {},
                {
                    headers: new HttpHeaders({
                        Authorization: `${this.user.id}`,
                    }),
                }
            )
            .pipe(catchError(this.errorHandler));
    }

    getCurrentUser(): Observable<any> {
        return this.currentUserSubject.asObservable();
    }

    isLoggedIn() {
        return !!localStorage.getItem('userInfo');
    }
}
