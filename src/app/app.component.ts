import { Component } from '@angular/core';
import { Router } from '@angular/router';
import User from './models/User';
import { AuthService } from './services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    title = 'surabi-admin';
    user!: User | null;
    response: Object = {};
    error: any = null;
    loading: boolean = false;

    constructor(private authService: AuthService, private router: Router) {
        this.authService.getCurrentUser().subscribe((data: User) => {
            this.user = data;
        });
    }

    logout() {
        this.loading = true;
        this.authService.logout().subscribe(
            (data) => {
                this.response = data;
                this.user = null;
                this.loading = false;
                localStorage.removeItem('userInfo');
                this.router.navigate(['/']);
            },
            (error) => {
                this.error = error;
                this.loading = false;
            }
        );
    }
}
