import { Component, OnInit } from '@angular/core';

import { ReportService } from 'src/app/services/report.service';

import Report from '../../models/Report';
@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.css'],
})
export class ReportComponent implements OnInit {
    error: any = null;
    loading: boolean = false;
    report!: Report;

    constructor(private reportService: ReportService) {}

    ngOnInit(): void {
        this.loading = true;
        this.reportService.getMonthsReport().subscribe(
            (data: any) => {
                this.report = data;
                this.loading = false;
            },
            (error) => {
                this.error = error;
                this.loading = false;
            }
        );
    }
}
