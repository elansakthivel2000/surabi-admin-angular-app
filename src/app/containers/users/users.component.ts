import { Component, OnInit } from '@angular/core';
import User from 'src/app/models/User';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
    loading: boolean = false;
    users: User[] = [];
    error: any = null;
    username: string = '';
    password: string = '';
    successMessage: any = null;
    isEditing: boolean[] = [];
    curUser!: User;

    updateUserData: any;
    constructor(
        private userService: UsersService,
        private authService: AuthService
    ) {
        this.authService.getCurrentUser().subscribe((data: User) => {
            this.curUser = data;
        });
    }

    ngOnInit(): void {
        this.loading = true;
        this.userService.getAllUsers().subscribe(
            (data: User[]) => {
                this.users = data;
                this.loading = false;
            },
            (error) => {
                this.error = error;
                this.loading = false;
            }
        );
    }

    setIsEditing(index: number) {
        this.isEditing[index] = true;
    }

    turnOffEditing(index: number) {
        this.isEditing[index] = false;
    }

    createNewUser() {
        this.loading = true;
        this.successMessage = null;
        this.error = null;
        this.userService.addUser(this.username, this.password).subscribe(
            (data) => {
                this.loading = false;
                this.successMessage = 'New User successfully created.';
                this.users.unshift(data);
                this.username = '';
                this.password = '';
            },
            (error) => {
                this.error = error;
                this.loading = false;
            }
        );
    }

    updateUser(updateData: any) {
        const { username, userId, index } = updateData;
        this.loading = true;
        this.successMessage = null;
        this.error = null;
        this.userService.updateUser(username, userId).subscribe(
            (data) => {
                this.loading = false;
                this.successMessage = 'Updated user Successfully.';

                this.users = this.users.map((user) => {
                    if (user.id !== userId) {
                        return user;
                    }
                    return {
                        ...user,
                        username: username,
                    };
                });

                this.turnOffEditing(index);
            },
            (error) => {
                this.error = error;
                this.loading = false;
            }
        );
    }

    deleteUser(userId: number) {
        this.loading = true;
        this.successMessage = null;
        this.error = null;
        this.userService.deleteUser(userId).subscribe(
            (data) => {
                this.loading = false;
                this.users = this.users.filter((user) => user.id !== userId);
                this.successMessage = 'User successfully deleted.';
            },
            (error) => {
                this.error = error;
                this.loading = false;
            }
        );
    }
}
