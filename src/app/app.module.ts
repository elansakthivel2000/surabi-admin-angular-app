import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AuthComponent } from './containers/auth/auth.component';
import { BillsComponent } from './containers/bills/bills.component';
import { ReportComponent } from './containers/report/report.component';

import { BillsService } from './services/bills.service';
import { UsersComponent } from './containers/users/users.component';
import { EditComponent } from './components/edit/edit.component';
import { AuthService } from './services/auth.service';
import { UsersService } from './services/users.service';
import { ReportService } from './services/report.service';
import { AuthGuard } from './auth.guard';
import { NotLoggedInGuardGuard } from './not-logged-in-guard.guard';

@NgModule({
    declarations: [
        AppComponent,
        AuthComponent,
        BillsComponent,
        ReportComponent,
        UsersComponent,
        EditComponent,
    ],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
    providers: [
        BillsService,
        AuthService,
        UsersService,
        ReportService,
        AuthGuard,
        NotLoggedInGuardGuard,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
