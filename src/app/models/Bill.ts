import Order from './Order';

interface Bill {
    id: number;
    billAmount: number;
    numItems: number;
    purchasedAt: string;
    orders: Order[];
}

export default Bill;
