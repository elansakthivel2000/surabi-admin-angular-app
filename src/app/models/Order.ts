import Product from './Product';

interface Order {
    id: number;
    productCount: number;
    product: Product;
}

export default Order;
