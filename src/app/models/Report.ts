interface Report {
    year: number;
    month: number;
    totalSale: number;
    numSales: number;
}
export default Report;
