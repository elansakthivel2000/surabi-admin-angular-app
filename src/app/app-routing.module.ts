import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { AuthComponent } from './containers/auth/auth.component';
import { BillsComponent } from './containers/bills/bills.component';
import { ReportComponent } from './containers/report/report.component';
import { UsersComponent } from './containers/users/users.component';
import { NotLoggedInGuardGuard } from './not-logged-in-guard.guard';

const routes: Routes = [
    {
        path: '',
        component: AuthComponent,
        canActivate: [NotLoggedInGuardGuard],
    },
    { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
    {
        path: 'todaysBills',
        component: BillsComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'monthsReport',
        component: ReportComponent,
        canActivate: [AuthGuard],
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
