import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
    @Input()
    username!: string;

    @Input()
    userId!: number;

    @Input()
    index!: number;

    @Output()
    turnOffIsEditing = new EventEmitter();

    @Output()
    updateUser = new EventEmitter();

    constructor() {}

    ngOnInit(): void {}
}
